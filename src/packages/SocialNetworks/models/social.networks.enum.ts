/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity
 * Identity Management Service.
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */

/**
 * Predefined social networks type
 */
export enum SOCIAL_NETWORK_TYPES {
  SN_UNKNOWN = 0,
  SN_FACEBOOK = 1,
  SN_GOOGLE = 2,
  SN_TWITTER = 3,
  SN_INSTAGRAM = 4,
  SN_YAHOO = 5,
  SN_LINKEDIN = 6
}
