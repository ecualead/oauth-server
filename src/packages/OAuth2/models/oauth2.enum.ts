/**
 * Copyright (C) 2020 IKOA Business Opportunity
 * All Rights Reserved
 * Author: Reinier Millo Sánchez <millo@ikoabo.com>
 *
 * This file is part of the IKOA Business Opportunity
 * Identity Management Service.
 * It can't be copied and/or distributed without the express
 * permission of the author.
 */

export enum OAUTH2_TOKEN_TYPE {
  TT_UNKNOWN = 0,
  TT_MODULE = 1,
  TT_APPLICATION = 2,
  TT_USER = 3,
  TT_USER_SOCIAL = 4
}

export const DEFAULT_SCOPES: string[] = [
  "default",
  "non_user",
  "module",
  "application",
  "social",
  "application_owner",
  "project_owner",
  "user"
];
